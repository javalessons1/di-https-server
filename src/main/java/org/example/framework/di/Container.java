package org.example.framework.di;

import lombok.extern.slf4j.Slf4j;
import org.example.framework.di.annotation.Autowired;
import org.example.framework.di.annotation.Component;
import org.example.framework.di.exception.*;
import org.example.framework.server.processor.BeanPostProcessor;
import org.reflections.Reflections;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.*;
import java.util.stream.Collectors;

// TODO:
//  container.register("manager", Manager.class)
//  container.register("repository", Repository.class)
//  container.register("ds", DataSource.class)
//  container.refresh() -> wire
@Slf4j
public class Container {
  private final Map<String, Class<?>> definitions = new HashMap<>();
  private final Map<String, Object> beans = new HashMap<>();
  private final List<BeanPostProcessor> postProcessors = new ArrayList<>();
  private boolean wired = false;

  public void register(final Class<?> clazz) {
    register(clazz.getName(), clazz);
  }

  public void register(final Object bean) {
      register(bean.getClass().getName(), bean);
  }

  public synchronized void register(final String name, final Class<?> clazz) {
    // TODO: check if already registered
    assertNotWired();
    if (BeanPostProcessor.class.isAssignableFrom(clazz)) {
      postProcessors.add((BeanPostProcessor) nextGeneration(clazz));
    }
    definitions.put(name, clazz);
  }

  public synchronized void register(final String name, final Object bean) {
    // TODO: check if already registered
    assertNotWired();
    definitions.put(name, bean.getClass());
    if (bean instanceof BeanPostProcessor) {
      postProcessors.add((BeanPostProcessor) bean);
    }
    beans.put(name, bean);

  }
  public synchronized void register(final String packageName) {
    // TODO: check if already registered
    final Reflections reflections = new Reflections(packageName);
    final Set<Class<?>> classes = reflections.getTypesAnnotatedWith(Component.class);
    for (Class<?> clazz : classes) {
      assertNotWired();
      definitions.put(clazz.getName(), clazz);
    }

  }

  public synchronized void wire() {
    assertNotWired();
    wired = true;

    doWire();

    final List<Object> autowireBeans = getBeansByAnnotation(Autowired.class);
    for (final Object autowireBean : autowireBeans) {
      final List<Method> methods = Arrays.stream(autowireBean.getClass().getDeclaredMethods()).filter(
              o -> o.isAnnotationPresent(Autowired.class)).collect(Collectors.toList());
      for (final Method method : methods) {
        final Parameter[] parameters = method.getParameters();
        for (final Parameter parameter : parameters) {
          // TODO: handle not-collection types
          if (List.class.isAssignableFrom(parameter.getType())) {
            final ParameterizedType parameterizedType = (ParameterizedType) parameter.getParameterizedType();
            final Type[] actualTypeArguments = parameterizedType.getActualTypeArguments();
            if (actualTypeArguments.length != 1) {
              continue;
            }

            final List<Object> beans = getBeansByType((Class<Object>) actualTypeArguments[0]);
            try {
              method.invoke(autowireBean, beans);
            } catch (IllegalAccessException | InvocationTargetException e) {
              throw new AutowireException(e);
            }
          }
        }
      }
    }
  }

  public synchronized Object getBean(final String name) {
    assertWired();

    return Optional.ofNullable(beans.get(name)).orElseThrow(() -> new BeanNotFoundException(name));
  }

  public <T> T getBean(final String name, final Class<T> clazz) {
    return (T) getBean(name);
  }

  public <T> T getBean(final Class<T> clazz) {
    // TODO: надо побегать по всем и найти тех, кто isAssignableFrom (и если их больше одного - то Exception)
    return getBean(clazz.getName(), clazz);
  }

  public synchronized List<Object> getBeansByAnnotation(Class<? extends Annotation> annotationClazz) {
    assertWired();
    return beans.values().stream()
        .filter(o -> o.getClass().isAnnotationPresent(annotationClazz))
        .collect(Collectors.toList());
  }

  public synchronized <T> List<T> getBeansByType(final Class<T> typeClazz) {
    assertWired();
    return beans.values().stream()
        .filter(o -> typeClazz.isAssignableFrom(o.getClass()))
        .map(o -> (T) o)
        .collect(Collectors.toList());
  }

  private void doWire() {
    while (true) {
      int count = 0;

      for (final Map.Entry<String, Class<?>> entry : definitions.entrySet()) {
        final String name = entry.getKey();
        final Class<?> clazz = entry.getValue();
        log.debug("try to instantiate: {}", name);
        Object bean = nextGeneration(clazz);
        if (bean == null) {
          continue;
        }
        final Class<?> originalClazz = bean.getClass();

        log.debug("bean created: {}, {}", name, clazz.getName());
        for (BeanPostProcessor postProcessor : postProcessors){
          bean = postProcessor.process(bean, originalClazz);
        }
        beans.put(name, bean);

        count++;
      }
      log.debug("after next generation: {}", beans.keySet());

      if (definitions.size() == beans.size()) {
        return;
      }

      if (count == 0) {
        throw new UnmetDependenciesException();
      }
    }
  }

  private void assertWired() {
    if (!wired) {
      throw new InvalidContainerStateException("not wired");
    }
  }

  private void assertNotWired() {
    if (wired) {
      throw new InvalidContainerStateException("already wired");
    }
  }

  private Optional<?> instantiate(final Parameter parameter) {
    final Class<?> paramClazz = parameter.getType();
    return beans.values().stream()
        .filter(o -> paramClazz.isAssignableFrom(o.getClass()))
        .findFirst()
        ;
  }

  private boolean isInstantiated(Class<?> clazz) {
    return beans.values().stream()
        .anyMatch(o -> clazz.equals(o.getClass()));
  }

  private Object nextGeneration(final Class<?> clazz) {
    if (isInstantiated(clazz)) {
      return null;
    }

    final Constructor<?>[] constructors = clazz.getConstructors();
    if (constructors.length != 1) {
      try {
        // TODO: refactor
        constructors[0] = clazz.getConstructor();
      } catch (NoSuchMethodException e) {
        throw new AmbigiousConstructorException("bean must have only one public constructor or default constructor: " + clazz.getName(), e);
      }
    }

    final Constructor<?> constructor = constructors[0];
    final Parameter[] parameters = constructor.getParameters();
    final List<Object> args = new ArrayList<>(parameters.length);
    // TODO: stream().map
    for (final Parameter parameter : parameters) {
      final Optional<?> arg = instantiate(parameter);
      if (!arg.isPresent()) {
        return null;
      }
      args.add(arg.get());
    }

    try {
      Object bean = constructor.newInstance(args.toArray());
      final Class<?> OriginalBeanClass = bean.getClass();

      for (final BeanPostProcessor processor: postProcessors) {
        bean = processor.process(bean, OriginalBeanClass, args.toArray());
      }

      return bean;

    } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
      throw new BeanInstantiationException(e);
    }
  }
}
