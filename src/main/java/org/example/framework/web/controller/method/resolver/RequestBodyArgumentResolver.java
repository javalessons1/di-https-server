package org.example.framework.web.controller.method.resolver;

import org.example.framework.di.annotation.Autowired;
import org.example.framework.server.annotation.RequestBody;
import org.example.framework.server.controller.method.converter.HttpMessageConverter;
import org.example.framework.server.controller.method.resolver.ArgumentResolver;
import org.example.framework.server.exception.NoSupportingMessageConverter;
import org.example.framework.server.exception.UnsupportedParameterException;
import org.example.framework.server.http.MediaTypes;
import org.example.framework.server.http.Request;

import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;

@Autowired
public class RequestBodyArgumentResolver implements ArgumentResolver {
  private final List<HttpMessageConverter> messageConverters = new ArrayList<>();

  @Autowired
  public void setMessageConverters(final List<HttpMessageConverter> messageConverters) {
    this.messageConverters.addAll(messageConverters);
  }

  @Override
  public boolean supportsParameter(final Parameter parameter) {
    return parameter.isAnnotationPresent(RequestBody.class);
  }

  @Override
  public Object resolveArgument(final Parameter parameter, final Request request) throws Exception {
    if (!supportsParameter(parameter)) {
      // this should never happen
      throw new UnsupportedParameterException(parameter.getName());
    }
    final Class<?> paramClazz = parameter.getType();

    final MediaTypes mediaType = request.getContentType();
    for (final HttpMessageConverter messageConverter : messageConverters) {
      if (messageConverter.canRead(paramClazz, mediaType)) {
        return messageConverter.read(paramClazz, request);
      }
    }

    throw new NoSupportingMessageConverter(mediaType.value());
  }
}
