package org.example.framework.server.util;

import java.util.Objects;

public class Bytes {
  public static int indexOf(byte[] array, byte[] target) {
    return indexOf(array, target, 0);
  }

  /**
   * Based on Google Guava Bytes.indexOf
   * TODO: license & stuff question
   */
  public static int indexOf(byte[] array, byte[] target, int offset) {
    Objects.requireNonNull(array);
    Objects.requireNonNull(target);
    if (target.length == 0) {
      return 0;
    }

    outer:
    for (int i = offset; i < array.length - target.length + 1; i++) {
      for (int j = 0; j < target.length; j++) {
        if (array[i + j] != target[j]) {
          continue outer;
        }
      }
      return i;
    }
    return -1;
  }
}
