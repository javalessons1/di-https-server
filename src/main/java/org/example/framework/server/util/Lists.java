package org.example.framework.server.util;

import java.util.Collections;
import java.util.List;

public class Lists {
  private Lists() {
  }

  public static <E> List<E> of() {
    return Collections.emptyList();
  }

  public static <E> List<E> of(E element) {
    return Collections.singletonList(element);
  }
}
