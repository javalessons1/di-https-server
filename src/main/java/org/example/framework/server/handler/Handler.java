package org.example.framework.server.handler;

import org.example.framework.server.http.MediaTypes;
import org.example.framework.server.http.Request;
import org.example.framework.server.http.Response;

import java.io.IOException;

@FunctionalInterface
public interface Handler {
  void handle(final Request request, final Response response) throws IOException;

  static void internalServerError(final Request request, final Response response) throws IOException {
    response.write(500, "Internal Server Error", MediaTypes.TEXT_PLAIN, "Something Bad Happened");
  }

  static void notFoundHandler(final Request request, final Response response) throws IOException {
    response.write(404, "Resource not found", MediaTypes.TEXT_PLAIN, "Resource not found");
  }

  static void methodNotAllowedHandler(final Request request, final Response response) throws IOException {
    response.write(405, "Method Not Allowed", MediaTypes.TEXT_PLAIN, "Method Not Allowed");
  }
}
