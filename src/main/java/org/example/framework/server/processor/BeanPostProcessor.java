package org.example.framework.server.processor;

public interface BeanPostProcessor {
    Object process(Object bean, Class<?> clazz, Object... args);
}

