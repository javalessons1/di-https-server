package org.example.framework.server.controller;

import org.example.framework.server.annotation.Controller;
import org.example.framework.server.annotation.RequestMapping;
import org.example.framework.server.exception.InvalidControllerBeanException;
import org.example.framework.server.http.HttpMethods;
import org.example.framework.server.router.MethodRouter;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class ControllerRegistrar {
  public MethodRouter register(final List<?> beans) {
    final MethodRouter route = new MethodRouter();
    for (final Object bean : beans) {
      register(bean, route);
    }
    return route;
  }

  private void register(final Object bean, final MethodRouter route) {
    final Class<?> clazz = bean.getClass();
    if (!clazz.isAnnotationPresent(Controller.class)) {
      throw new InvalidControllerBeanException("annotation @Controller didn't present: " + clazz.getName());
    }

    final List<Method> methods = Arrays.stream(clazz.getMethods())
        .filter(o -> o.isAnnotationPresent(RequestMapping.class))
        .collect(Collectors.toList());

    for (final Method method : methods) {
      final RequestMapping requestMapping = method.getAnnotation(RequestMapping.class);
      final HttpMethods httpMethod = requestMapping.method();
      final String path = requestMapping.path();
      final Pattern pattern = Pattern.compile(path);
      route.register(httpMethod, pattern, bean, method);
    }
  }
}
