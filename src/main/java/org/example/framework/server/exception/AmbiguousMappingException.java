package org.example.framework.server.exception;

public class AmbiguousMappingException extends RuntimeException {
  public AmbiguousMappingException() {
  }

  public AmbiguousMappingException(String message) {
    super(message);
  }

  public AmbiguousMappingException(String message, Throwable cause) {
    super(message, cause);
  }

  public AmbiguousMappingException(Throwable cause) {
    super(cause);
  }

  public AmbiguousMappingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
