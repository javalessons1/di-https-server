package org.example.framework.server.exception;

public class InvalidHeaderLineStructureException extends InvalidRequestStructureException {
  public InvalidHeaderLineStructureException() {
  }

  public InvalidHeaderLineStructureException(String message) {
    super(message);
  }

  public InvalidHeaderLineStructureException(String message, Throwable cause) {
    super(message, cause);
  }

  public InvalidHeaderLineStructureException(Throwable cause) {
    super(cause);
  }

  public InvalidHeaderLineStructureException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
