package org.example.framework.server.exception;

public class BadAuthenticationException extends RuntimeException {
  public BadAuthenticationException() {
  }

  public BadAuthenticationException(String message) {
    super(message);
  }

  public BadAuthenticationException(String message, Throwable cause) {
    super(message, cause);
  }

  public BadAuthenticationException(Throwable cause) {
    super(cause);
  }

  public BadAuthenticationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
