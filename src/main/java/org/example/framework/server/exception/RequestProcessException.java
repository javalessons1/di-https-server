package org.example.framework.server.exception;

public class RequestProcessException extends RuntimeException {
  public RequestProcessException() {
  }

  public RequestProcessException(String message) {
    super(message);
  }

  public RequestProcessException(String message, Throwable cause) {
    super(message, cause);
  }

  public RequestProcessException(Throwable cause) {
    super(cause);
  }

  public RequestProcessException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
