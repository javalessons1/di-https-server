package org.example.framework.server.exception;

public class ResponseWriteException extends RuntimeException {
  public ResponseWriteException() {
  }

  public ResponseWriteException(String message) {
    super(message);
  }

  public ResponseWriteException(String message, Throwable cause) {
    super(message, cause);
  }

  public ResponseWriteException(Throwable cause) {
    super(cause);
  }

  public ResponseWriteException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
