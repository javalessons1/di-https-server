package org.example.framework.server.exception;

public class UnsupportedParameterTypeException extends RuntimeException {
  public UnsupportedParameterTypeException() {
  }

  public UnsupportedParameterTypeException(String message) {
    super(message);
  }

  public UnsupportedParameterTypeException(String message, Throwable cause) {
    super(message, cause);
  }

  public UnsupportedParameterTypeException(Throwable cause) {
    super(cause);
  }

  public UnsupportedParameterTypeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
