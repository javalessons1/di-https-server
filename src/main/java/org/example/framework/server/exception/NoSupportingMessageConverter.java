package org.example.framework.server.exception;

public class NoSupportingMessageConverter extends RuntimeException {
  public NoSupportingMessageConverter() {
  }

  public NoSupportingMessageConverter(String message) {
    super(message);
  }

  public NoSupportingMessageConverter(String message, Throwable cause) {
    super(message, cause);
  }

  public NoSupportingMessageConverter(Throwable cause) {
    super(cause);
  }

  public NoSupportingMessageConverter(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
