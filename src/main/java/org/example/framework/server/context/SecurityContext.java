package org.example.framework.server.context;

import java.security.Principal;

public class SecurityContext {
  private static final ThreadLocal<Principal> principalHolder = new ThreadLocal<>();
  private static final ThreadLocal<AuthRQ> securityContext = new ThreadLocal<>();

  private SecurityContext() {}

  public static void setPrincipal(final Principal principal) {
    principalHolder.set(principal);
  }

  public static Principal getPrincipal() {
    return principalHolder.get();
  }

  public static void clear() {
    principalHolder.remove();
  }
  public static void setAuth(final AuthRQ auth) {
    securityContext.set(auth);
  }

  public static AuthRQ getAuth() {
    return securityContext.get();
  }

}
