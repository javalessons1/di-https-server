package org.example.framework.server.context;

import lombok.*;

@Data
@Builder
public class AuthRQ {
    private final String login;
    private final String role;
}
