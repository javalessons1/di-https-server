package org.example.framework.server.http.parser;

import lombok.extern.slf4j.Slf4j;
import org.example.framework.server.http.Request;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class MultipartParser {

  public static Map<String, List<Part>> parseBody(final String body, final Request request) {
    Map<String, List<Part>> multipartParts = new LinkedHashMap<>();
    try {
      if (request.getContentType().equals("multipart/form-data")) {
        List<String> parts = getParts(body, request);
        for (String part : parts) {
          List<Part> tempList = new ArrayList<>();
          Part multipart = parseOnePart(part);
          boolean b = multipartParts.containsKey(multipart.getName());
          if (multipartParts.containsKey(multipart.getName())) {
            tempList = multipartParts.get(multipart.getName());
          }
          tempList.add(multipart);
          multipartParts.put(multipart.getName(), tempList);
        }
        log.debug("Body successfully parsed!");
        return multipartParts;
      }
      log.debug("Nothing to parse!");
      return multipartParts;
    }
    catch (Exception e) {
      log.debug("Can't parse!", e);
      throw new MultipartParseException();
    }

  }

  public static List<String> getParts(final String body, final Request request) {
    List<String> parts = new ArrayList<>();
    String[] temporaryParts;
    final String[] getBoundary = body.split("\r\n");
    final String boundary = getBoundary[0];
    temporaryParts = body.split(boundary);
    for (String part : temporaryParts) {
      final String[] partElems = part.split("\r\n\r\n");
      if (partElems.length > 1) {
        parts.add(part);
      }
    }
    return parts;
  }
  public static Part parseOnePart(String part){
    Part newFormPart;
    Part newFilePart;
    String headersKey;
    String headersValue;
    Map<String, String> headers = new LinkedHashMap<>();
    final String[] partElems = part.split("\r\n\r\n");
    final String[] headersToSplit = partElems[0].split("\r\n");
    for (final String headerPart : headersToSplit) {
      final String[] headerSplited = headerPart.split(":");
      if (headerSplited.length > 1) {
        headersKey = headerSplited[0];
        final String[] headersValueLine = headerSplited[1].split(";");
        headersValue = headersValueLine[0].replaceAll(" ", "");
        headers.put(headersKey, headersValue);
      }
    }
    Map<String, String> names = getNames(headersToSplit[1]);
    String value = partElems[1];

    if (names.containsValue("filename")) {
      newFilePart = new FilePart(names.get("name"), names.get("filename"), value.getBytes(), headers);
      return newFilePart;
    } else {
      newFormPart = new FormFieldPart(names.get("name"), value, headers);
      return newFormPart;
    }
  }

  public static Map<String, String> getNames(String headerLine){
    String name = "";
    Map<String, String> names = new LinkedHashMap<>();
    String[] nameLine = headerLine.split(";");
    for (final String nam : nameLine) {
      if (nam.contains(" name=")) {
        name = nam.split("=")[1].replaceAll("\"", "");
        names.put("name", name);
      }
      if (nam.contains(" filename=")) {
        final String[] filenameString = nam.split("=");
        String filename = filenameString[1].replaceAll("\"", "");
        names.put("filename", filename);
      }
    }
    return names;
  }
}
