package org.example.framework.server.http;

public enum HttpMethods {
  GET,
  POST,
  PUT,
  PATCH,
  DELETE;
}
