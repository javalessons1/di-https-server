package org.example.framework.server.http;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.example.framework.server.http.parser.Part;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Request {
  private String method;
  private String path;
  private Matcher pathMatcher;
  private String query;
  private final Map<String, List<String>> queryParams = new LinkedHashMap<>();
  private Map<String, List<Part>> multipartParts = new LinkedHashMap<>();
  private String httpVersion;
  private final Map<String, String> headers = new LinkedHashMap<>();
  private byte[] body;

  public String getPathGroup(String name) {
    return pathMatcher.group(name);
  }

  public String getPathGroup(int index) {
    return pathMatcher.group(index);
  }

  // TODO: GET /path?count=10&offset=20
  //  getQueryParam("sort") - null
  public Optional<String> getQueryParam(String name) {
    // Just demo realization
    return Optional.empty(); // Optional.ofNullable()
  }

  public String getQueryParamOrNull(String name) {
    return null;
  }

  public List<String> getQueryParams(String name) {
    throw new RuntimeException();
  }

  public Map<String, List<String>> getAllQueryParams() {
    return queryParams;
  }

  public MediaTypes getContentType() {
    return Optional.ofNullable(headers.get(HttpHeaders.CONTENT_TYPE.value()))
        .map(MediaTypes::fromValue)
        .orElse(MediaTypes.ANY)
        ;
  }

  // FIXME: допущение - клиент шлёт только один Accept: application/json,text/xml
  public MediaTypes getAccept() {
    return Optional.ofNullable(headers.get(HttpHeaders.ACCEPT.value()))
        .map(MediaTypes::fromValue)
        .orElse(MediaTypes.ANY);
  }

  public void setMultipartParts(Map<String, List<Part>> parts) {
    multipartParts = parts;
  }
  public Map<String, List<Part>> getMultipartParts() {
    return multipartParts;
  }

}
