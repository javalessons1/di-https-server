package org.example.framework.server.http.parser;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class FormFieldPart implements Part {
    String name;
    String value;
    Map<String, String> headers;


    @Override
    public String getName() {
        return name;
    }

    public String getValue() {
        return value;
    }

    @Override
    public Map<String, String> getHeaders() {
        return headers;
    }
}
