package org.example.framework.server.http.parser;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Slf4j
public class FilePart implements Part {
    String name;
    String filename;
    byte[] content;
    Map<String, String> headers;

    @Override
    public String getName() {
        return name;
    }

    public String getFileName() {
        return filename;
    }


    public byte[] getContent() {
        return content;
    }

    @Override
    public Map<String, String> getHeaders() {
        return headers;
    }
}
