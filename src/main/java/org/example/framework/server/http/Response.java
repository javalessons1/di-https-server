package org.example.framework.server.http;

import lombok.RequiredArgsConstructor;
import org.example.framework.server.exception.ResponseWriteException;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

@RequiredArgsConstructor
public class Response {
  private final OutputStream out;

  public void write(final MediaTypes mediaType, final String body) {
    // TODO: move to constants/enum
    write(200, "OK", mediaType, body.getBytes(StandardCharsets.UTF_8));
  }

  public void write(final int statusCode, final String statusText, final MediaTypes mediaType, final String body) {
    write(statusCode, statusText, mediaType, body.getBytes(StandardCharsets.UTF_8));
  }

  public void write(final MediaTypes mediaType, final byte[] body) {
    // TODO: move to constants/enum
    write(200, "OK", mediaType, body);
  }

  public void write(final int statusCode, final String statusText, final MediaTypes mediaType, final byte[] body) {
    try {
      out.write((
          "HTTP/1.1 " + statusCode + " " + statusText + "\r\n" +
          "Content-Length: " + body.length + "\r\n" +
          "Connection: close\r\n" +
          "Content-Type: " + mediaType.value() + "\r\n" +
          "\r\n"
      ).getBytes(StandardCharsets.UTF_8));
      out.write(body);
    } catch (IOException e) {
      throw new ResponseWriteException(e);
    }
  }
}
