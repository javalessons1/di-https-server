package org.example.framework.server.http.parser;

public class MultipartException extends RuntimeException {
  public MultipartException() {
  }

  public MultipartException(String message) {
    super(message);
  }

  public MultipartException(String message, Throwable cause) {
    super(message, cause);
  }

  public MultipartException(Throwable cause) {
    super(cause);
  }

  public MultipartException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
    super(message, cause, enableSuppression, writableStackTrace);
  }
}
