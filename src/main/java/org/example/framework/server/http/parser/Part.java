package org.example.framework.server.http.parser;

import java.util.Map;

public interface Part {
    String getName();

    Map<String, String> getHeaders();
}
